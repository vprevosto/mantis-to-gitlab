#=============================================================================
# Copyright 2015-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
import requests
import json

__all__ = [
    'Gitlab',
]


def _mkrequest(path, **defkwargs):
    def func(self, *args, **kwargs):
        data = defkwargs.copy()
        data.update(kwargs)

        return self.fetch(path.format(*args), **data)

    return func


def _mkrequest_paged(path, **defkwargs):
    def func(self, *args, **kwargs):
        data = defkwargs.copy()
        data.update(kwargs)

        return self.fetch_all(path.format(*args), **data)

    return func


def _mkpost(path, **defkwargs):
    def func(self, *args, **kwargs):
        data = defkwargs.copy()
        data.update(kwargs)

        return self.post(path.format(*args), **data)

    return func


def _mkput(path, **defkwargs):
    def func(self, *args, **kwargs):
        data = defkwargs.copy()
        data.update(kwargs)

        return self.put(path.format(*args), **data)

    return func


def _mkdelete(path, **defkwargs):
    def func(self, *args, **kwargs):
        data = defkwargs.copy()
        data.update(kwargs)

        return self.delete(path.format(*args), **data)

    return func


class Gitlab(object):
    def __init__(self, host, token):
        self.urlbase = '%s/api/v4' % (host)
        self.headers = {
            'PRIVATE-TOKEN': token,
        }

    def fetch(self, path, **kwargs):
        url = '%s/%s' % (self.urlbase, path)
        response = requests.get(url, headers=self.headers, params=kwargs)

        if response.status_code != 200:
            return False

        if callable(response.json):
            return response.json()
        else:
            return response.json

    def fetch_all(self, path, **kwargs):
        kwargs.update({
            'page': 1,
            'per_page': 100,
        })

        full = []
        prev = [] # XXX: gitlab bug variable
        while True:
            items = self.fetch(path, **kwargs)
            if not items:
                break

            # XXX: gitlab bug: paging API returns all results
            if items == prev:
                break
            prev = items
            # XXX

            full += items
            if len(items) < kwargs['per_page']:
                break
            kwargs['page'] += 1

        return full

    def post(self, path, sudo=None, **kwargs):
        url = '%s/%s' % (self.urlbase, path)
        if sudo is not None:
            headers = { 'Sudo': sudo }
            headers.update(self.headers)
        else:
            headers = self.headers
        response = requests.post(url, headers=headers, data=kwargs)
        if response.status_code != 201:
            print( 'post failed:', response.json() )
            return False

        if callable(response.json):
            return response.json()
        else:
            return response.json

    def put(self, path, sudo=None, **kwargs):
        url = '%s/%s' % (self.urlbase, path)
        if sudo is not None:
            headers = { 'Sudo': sudo }
            headers.update(self.headers)
        else:
            headers = self.headers
        response = requests.put(url, headers=headers, data=kwargs)

        if response.status_code != 200:
            return False

        if callable(response.json):
            return response.json()
        else:
            return response.json

    def delete(self, path, **kwargs):
        url = '%s/%s' % (self.urlbase, path)
        response = requests.delete(url, headers=self.headers, data=kwargs)

        if response.status_code == 204:
            return True

        if response.status_code != 200:
            return False

        if callable(response.json):
            return response.json()
        else:
            return response.json

    # Users
    currentuser = _mkrequest('user')
    getusers = _mkrequest_paged('users')
    getuser = _mkrequest('users/{}')
    edituser = _mkput('users/{}')
    createuser = _mkpost('users')
    blockuser = _mkpost('users/{}/block')

    # Projects
    getproject = _mkrequest('projects/{}')
    getaccessibleprojects = _mkrequest_paged('projects')
    getownedprojects = _mkrequest_paged('projects/owned')
    getallprojects = _mkrequest_paged('projects/all')
    getprojecthooks = _mkrequest_paged('projects/{}/hooks')
    getprojectmembers = _mkrequest_paged('projects/{}/members')
    addprojectteammember = _mkpost('projects/{}/members')
    getprojectteammember = _mkrequest('projects/{}/members/{}')
    getbranch = _mkrequest('projects/{}/repository/branches/{}')
    getprojectcommitcomment = _mkrequest_paged('projects/{}/repository/commits/{}/comments')
    createcommitcomment = _mkpost('projects/{}/repository/commits/{}/comments')
    addprojecthook = _mkpost('projects/{}/hooks')
    editprojecthook = _mkput('projects/{}/hooks/{}')
    delprojecthook = _mkdelete('projects/{}/hooks/{}')

    # Commit status
    getlatestcommitstatuses = _mkrequest_paged('projects/{}/repository/commits/{}/statuses')
    getallcommitstatuses = _mkrequest_paged('projects/{}/repository/commits/{}/statuses',
        all=True)
    setcommitstatus = _mkpost('projects/{}/statuses/{}')

    # Issues
    getmyissues = _mkrequest_paged('issues')

    getissues = _mkrequest_paged('projects/{}/issues')
    getissue = _mkrequest('projects/{}/issues/{}')
    createissue = _mkpost('projects/{}/issues')
    editissue = _mkput('projects/{}/issues/{}')
    deleteissue = _mkdelete('projects/{}/issues/{}')
    getissuewallnotes = _mkrequest_paged('projects/{}/issues/{}/notes')
    createissuewallnote = _mkpost('projects/{}/issues/{}/notes')
    createissuelink = _mkpost('projects/{}/issues/{}/links')
    # Milestones
    getmilestones = _mkrequest_paged('projects/{}/milestones')

    # Merge requests
    getmergerequests = _mkrequest_paged('projects/{}/merge_requests')
    getmergerequest = _mkrequest('projects/{}/merge_request/{}')
    getsortedmergerequests = _mkrequest_paged('projects/{}/merge_requests',
        order_by='updated_at',
        sort='desc')
    getmergerequestcomments = _mkrequest_paged('projects/{}/merge_request/{}/comments')
    getmergerequestwallnotes = _mkrequest_paged('projects/{}/merge_requests/{}/notes')
    getmergerequestchanges = _mkrequest('projects/{}/merge_request/{}/changes')
    createmergerequestwallnote = _mkpost('projects/{}/merge_requests/{}/notes')

    # Groups
    getgroupmembers = _mkrequest_paged('groups/{}/members')

    # uploads
    def uploads(self,project_id,filename,filecontent):
        url = f'{self.urlbase}/projects/{project_id}/uploads'
        files = { 'file':(filename,filecontent) }
        r = requests.post(url,headers=self.headers,files=files)
        if not r.ok:
            print ('upload failed: {}',r.json())
            return False

        if callable(r.json):
            return r.json()
        else:
            return r.json


    def editproject(self,project_id,args):
        url = f'{self.urlbase}/projects/{project_id}'
        r = requests.put(url,headers=self.headers,json=args)
        if not r.ok:
            print (f'project {project_id} was not updated: {r.json()}')
            return False
        if callable(r.json):
            return r.json()
        else:
            return r.json

    def getaccesslevel(self, project_id, user_id):
        members = self.getprojectmembers(project_id)

        project = self.getproject(project_id)
        if project and 'namespace' in project:
            group_members = self.getgroupmembers(project['namespace']['id'])
            members += group_members

        access = 0
        for perm in filter(lambda member: user_id == member['id'], members):
            if perm['access_level'] > access:
                access = perm['access_level']
        return access

    def getaccesslevel_cache(self, cache, project_id, user_id):
        key = '%d,%d' % (project_id, user_id)
        if key not in cache:
            cache[key] = self.getaccesslevel(project_id, user_id)
        return cache[key]
