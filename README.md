mantis-to-gitlab
================

This script is meant to be used on a CSV export from a Mantis instance in
order to import the issues into GitLab. The target repo does not need to be
empty, but no attempt is made to keep the issue numbers synchronized between
mantis and gitlab. Since csv files exported by mantis 1.13 do not contain all
information (notably attachments and links between issues), the script also
requires a SOAP access token to the mantis server.

This software was originally written by
[kitware](https://gitlab.kitware.com/utils/mantis-to-gitlab) and is being
updated to work with the recent versions of gitlab (11/2018) and to copy more
data than the basic things it did. More details to follow.
