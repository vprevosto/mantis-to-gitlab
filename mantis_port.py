import gitlab

import csv
import re

import requests

from zeep import Client as SOAPClient, Settings as SOAPSettings
import zeep.exceptions

try:
    from urllib import quote  # Python 2.X
except ImportError:
    from urllib.parse import quote  # Python 3+

class MantisIssues(object):
    def __init__(self, fin, url, id_project, user_name,token):
        self._issues = {}
        self.id_project = id_project
        self.user_name=user_name
        self.token = token
        self._max_issue_id = -1
        csv.field_size_limit(1048576)

        for issue in csv.DictReader(fin):
            issue_id = int(issue['Id'])
            issue['Id'] = issue_id
            issue['__url'] = '%s/view.php?id=%d' % (url, issue_id)
            self._max_issue_id = max(issue_id, self._max_issue_id)
            self._issues[issue_id] = issue

        self._max_issue_id += 1
        if not self._max_issue_id:
            raise RuntimeError('no issues?')
        full_url = '%s/api/soap/mantisconnect.php?wsdl'%url
        self.soap_client = SOAPClient(full_url)

        self._closed_statuses = set()
        self._users = {}
        self._categories = {}
        self._priorities = {}
        self._severities = {}
        self._projects = {}
        self._statuses = {}

    @property
    def max_id(self):
        return self._max_issue_id

    def add_issue_mapping(self, issue, gid):
        issue['__gitlab_id'] = gid

    def get_gitlab_id(self,issue):
        if issue['__gitlab_id'] is not None:
            issue['__gitlab_id']
        else:
          raise RuntimeError('no associated gitlab id for issue %d (not added to gitlab?)', issue['Id'])

    def add_category_mapping(self, category, labels):
        self._categories[category] = set(labels)

    def add_priority_mapping(self, priority, labels):
        self._priorities[priority] = set(labels)

    def add_severity_mapping(self, severity, labels):
        self._severities[severity] = set(labels)

    def add_project_mapping(self, project, labels):
        self._projects[project] = set(labels)

    def add_status_mapping(self, status, labels):
        self._statuses[status] = set(labels)

    def get_labels(self, issue):
        clabels = self._categories.get(issue.get('Category'), set())
        plabels = self._priorities.get(issue.get('Priority'), set())
        slabels = self._severities.get(issue.get('Severity'), set())
        jlabels = self._projects.get(issue.get('Project'), set())
        dlabels = self._statuses.get(issue.get('Status'), set())

        return clabels | plabels | slabels | jlabels | dlabels

    def add_user_mapping(self, name, gitlab_user):
        self._users[name] = gitlab_user

    def username(self, name):
        return self._users.get(name)

    def add_closed_status(self, status=None, resolution=None):
        if status is None and resolution is None:
            raise RuntimeError('at least a status or resolution must be provided')
        self._closed_statuses.add((status, resolution))

    def is_closed(self, issue):
        status, resolution = issue['Status'], issue.get('Resolution')

        for cstatus, cresolution in self._closed_statuses:
            smatch = cstatus     is None or cstatus     == status
            rmatch = cresolution is None or cresolution == resolution
            if smatch and rmatch:
                return True
        return False

    def get_attachments(self, issue_id):
        attached_files = [ ]
        try:
            # zeep insists on having proper username and password in the call,
            # although we don't need them thanks to the authentication in the
            # header. Fortunately, setting them to empty strings somehow works.
            issue = self.soap_client.service.mc_issue_get(username=self.user_name,password=self.token,issue_id=issue_id)
            for attachment in issue.attachments:
                content = self.soap_client.service.mc_issue_attachment_get(issue_attachment_id=attachment.id,username=self.user_name,password=self.token)
                attached_files.append((attachment.filename,content))
            return attached_files
        except zeep.exceptions.Error as e:
                          print (f'exception occurred when retrieving attachments from {issue_id}: {e}')
                          return attached_files

    def get_relations(self, issue_id):
        try:
            issue = self.soap_client.service.mc_issue_get(username=self.user_name,password=self.token,issue_id=issue_id)
            relations = []
            if issue.relationships is not None:
                for rel in issue.relationships:
                    relations.append((rel.type.name,rel.target_id))
            return relations
        except zeep.exceptions.Error as e:
                          print (f'exception occurred when retrieving relationships from {issue_id}: {e}')
                          return []

    def __getitem__(self, issue_id):
        return self._issues.get(issue_id)

    def __contains__(self, issue_id):
        return issue_id in self._issues

    def __iter__(self):
        self.__iterator = self._issues.__iter__()
        return self

    def __next__(self):
        next_id = self.__iterator.__next__()
        next_issue = self._issues.get(next_id)
        return next_issue

class Mantis2Gitlab(object):

    def __enter__(self):
        ret = gl.editproject(self.project_id, {'emails_disabled':True})
        if ret and not ret['emails_disabled']:
            print("disabling notifications failed")
        return self

    def __exit__(self,exn_type,exn_val,bt):
        gl.editproject(self.project_id,{'emails_disabled':False})
        if self.default_user is not None:
            user = self._get_user(self.default_user)
            gl.blockuser(user['id'])
        if exn_type is not None:
            print(f'An {exn_type} exception was raised: {exn_val}')
            print(f'traceback:\n{bt}')

    def __init__(self, gl, project, dry_run=False, sudo=False, ofile=None):
        self.gl = gl
        self._user_cache = {}
        self.dry_run = dry_run
        self.sudo = sudo
        self.re_note_header = re.compile('@(?P<author>[^(]+) (?P<private>\(private\) )?(?P<date>[0-9]{4}-[0-9]{2}-[0-9]{2}) (?P<time>[0-9]{2}:[0-9]{2})')
        project = self.gl.getproject(quote(project, ''))
        if not project:
            raise RuntimeError('target project does not exist!')
        self.project_id = project['id']
        self.milestones_info = gl.getmilestones(project['id'])
        self.default_user = None
        self.issues_csv = ofile

    def makeAnonymous(self):
        if self.default_user is not None:
            return self.default_user
        if self.sudo:
            self.default_user = 'mantis-gitlab-migration'
            exists = self.gl.getusers(username=self.default_user)
            if exists: return self.default_user
            ret = gl.createuser(email='mantis-gitlab-migration@example.com',
                                username=self.default_user,
                                name=self.default_user,
                                reset_password=True,
                                force_random_password=True)
            if not ret:
                print("Unable to create default user")
                self.default_user = None
        return self.default_user

    def _get_user(self, username):
        if username not in self._user_cache:
            user = self.gl.getusers(username=username)
            if not user:
                raise RuntimeError('unknown user %s' % username)
            self._user_cache[username] = user[0]
        return self._user_cache[username]


    def _create_gitlab_issue(self, data):
        if self.dry_run:
            import pprint
            pprint.pprint(data)
            return {'id': -1}
        else:
            issue = self.gl.createissue(self.project_id, **data)
            if not issue:
                raise RuntimeError('failed to create issue %d' % issue_id)
            return issue

    def _createissuewallnote(self, iid_issue,**kwargs):
        if self.dry_run:
            import pprint
            pprint.pprint(data)
            return {'id': -1, 'iid': -1}
        else:
            notes = self.gl.createissuewallnote(self.project_id, iid_issue, **kwargs)
            if not notes:
                raise RuntimeError('failed to create notes %d / %d' % (issue_id,iid_issue))
            return notes

    def _IfIsNotEmpty(this,issue,keystr):
        if keystr in issue :
            if len( issue[keystr] ) == 0 :
                return ''
            else:
                return "\n### " + keystr + " : \n" +  issue[keystr]
        else:
            return ''

    def _IfIsNotEmpty1(self,issue,keystr):
        if keystr in issue :
            if len( issue[keystr] ) == 0 :
                return ' - |'
            else:
                return " " +  issue[keystr] + " |"
        else:
            return ' - |'

    def _IfIsNotEmpty2(self,issue,keystr):
        if keystr in issue :
            if len( issue[keystr] ) == 0 :
                return ' **%s** | - |' % keystr
            else:
                return  ' **%s** | %s |' % ( keystr,  issue[keystr])
        else:
            return  ' **%s** | - |' % keystr

    def _createArrayMetaData(self,issue):
        strarray = '| **Id** | **Project** | **Category** | **View** | **Due Date** | **Updated** |\n'
        strarray = strarray + '| --- | --- | --- | --- | --- | --- |\n'
        strarray = strarray + '| ID%07d |' % issue['Id']
        strarray = strarray + self._IfIsNotEmpty1(issue,'Project')
        strarray = strarray + self._IfIsNotEmpty1(issue,'Category')
        strarray = strarray + self._IfIsNotEmpty1(issue,'View Status')
        strarray = strarray + self._IfIsNotEmpty1(issue,'Date Submitted')
        strarray = strarray + self._IfIsNotEmpty1(issue,'Updated')
        strarray = strarray + '\n\n'

        strarray = strarray + '|  |  |  |  |  |  |\n'
        strarray = strarray + '| --- | --- | --- | --- | --- | --- |\n'
        strarray = strarray + '|'  + self._IfIsNotEmpty2(issue,'Reporter')
        strarray = strarray        + self._IfIsNotEmpty2(issue,'Assigned To') 
        strarray = strarray        + self._IfIsNotEmpty2(issue,'Resolution') + '\n'

        strarray = strarray + '|'  + self._IfIsNotEmpty2(issue,'Priority')
        strarray = strarray        + self._IfIsNotEmpty2(issue,'Severity')
        strarray = strarray        + self._IfIsNotEmpty2(issue,'Reproducibility') + '\n'

        strarray = strarray + '|'  + self._IfIsNotEmpty2(issue,'Platform')
        strarray = strarray        + self._IfIsNotEmpty2(issue,'OS')
        strarray = strarray        + self._IfIsNotEmpty2(issue,'OS Version') + '\n'

        strarray = strarray + '|'  + self._IfIsNotEmpty2(issue,'Product Version')
        strarray = strarray        + self._IfIsNotEmpty2(issue,'Target Version')
        strarray = strarray        + self._IfIsNotEmpty2(issue,'Fixed in Version') + '\n'
        strarray = strarray + '\n'
        
        return strarray

    def getAuthor(self,issues,mantis_author):
        author = issues.username(mantis_author)
        if not author:
            print(f'Warning: {mantis_author} is unknown in gitlab')
            return self.makeAnonymous()
        return author

    def _create_issue(self, issues, issue):
        mantis_link_text = \
            '**This issue was created automatically ' \
            'from Mantis Issue %(Id)d. ' \
            'Further discussion may take place here.**' \
            % issue

        # Traitements simples
        description = 'ID%07d:\n' % issue['Id']
        description = description + mantis_link_text +'\n\n---\n\n'
        description = description + self._createArrayMetaData(issue)
        description = description + self._IfIsNotEmpty(issue,'Description' )
        description = description + self._IfIsNotEmpty(issue,'Tags'                  )
        description = description + self._IfIsNotEmpty(issue,'Additional Information')
        description = description + self._IfIsNotEmpty(issue,'Steps To Reproduce'    )
        attachments = issues.get_attachments(issue['Id'])
        if len(attachments) > 0:
            description = description + "\n\n## Attachments\n"
            for (name,content) in attachments:
                gitlab_file = self.gl.uploads(self.project_id,name,content)
                if gitlab_file is not False:
                    description = description + f"- {gitlab_file['markdown']}\n"
                else:
                    print(f'could not upload {name}')

        # translation du numero de target version en milestone
        milestone_translate={}
        for milestone in self.milestones_info :
            milestone_translate[milestone['title']]=milestone['id']
        milestone_id=0
        if issue['Target Version'] in milestone_translate :
            milestone_id= milestone_translate[issue['Target Version']]
        
        issue_data = {
                'id' : issues.id_project,
                'title': issue['Summary'],
                'milestone_id':milestone_id,
#                'assignee_ids':AssignedTo_id,
                'description': description,
                'created_at': '%(Date Submitted)sT00:00:00Z' % issue,
            }
        author = self.getAuthor(issues,issue['Reporter'])
        if self.sudo:
            if author is not None:
                issue_data['sudo'] = author
        if 'Due Date' in issue :
            if len( issue['Due Date'] ) != 0 and issue['Due Date'] != "1970-01-01":
                issue_data['due_date']=issue['Due Date']

        if issue['View Status'] == 'private':
            issue_data['confidential'] = 'true'

        is_closed = issues.is_closed(issue)
        if not is_closed and 'Assigned To' in issue:
            username = issues.username(issue['Assigned To'])
            if username:
                user = self._get_user(username)
                issue_data['assignee_id'] = user['id']

        labels = issues.get_labels(issue)
        issue_data['labels'] = ','.join(labels)

        print("Creating Issue No " + str(issue['Id']))
        if self.sudo and author: self.make_admin(author)
        try:
            gissue = self._create_gitlab_issue(issue_data)
        finally:
            if self.sudo and author: self.make_normal(author)
        issues.add_issue_mapping(issue,gissue['iid'])
        self._create_note(issues,issue,gissue['iid'])
        self._closed_issues(issues,issue,gissue['iid'])

    def make_admin(self,user):
        info = self._get_user(user)
        if not info['is_admin']:
            self.gl.edituser(info['id'],admin=True)

    def make_normal(self,user):
        info = self._get_user(user)
        if not info['is_admin']:
            self.gl.edituser(info['id'],admin=False)

    def _create_note(self, issues, issue, iid_issue):
        # Traitement des notes
        allnotes = issue['Notes']
        ii=1
        while allnotes != '':
            (current,sep,allnotes) = allnotes.partition('\n---\n')
            while allnotes != '' and allnotes[0] != '@':
                current = current + sep
                (chunk,sep,allnotes) = allnotes.partition('\n---\n')
                current = current + chunk
            print("Note " + str(ii) + " Issue No " + str(issue['Id']) +" -> " + str(iid_issue) )
            (header,rest) = current.split('\n',1)
            info = re.match(self.re_note_header,header)
            user = None
            if info is not None:
                body = f"### Original note by {info['author']}\n{rest}"
                date =  f"{info['date']}T{info['time']}:00Z"
                note_data = { 'body': body, 'created_at': date}
                if info['private'] is not None:
                    note_data['confidential'] = 'true'
                if self.sudo:
                    user=self.getAuthor(issues,info['author'])
                    if user is not None:
                        note_data['sudo']=user
            else:
                print(f"Warning: could not retrieve author from note {ii} of issue {issue['Id']}")
                note_data = { 'body': "\n\n\n### Note by " + current }
            ii = ii +  1
            if user is not None: self.make_admin(user)
            try :
                self._createissuewallnote(iid_issue,**note_data)
            except:
                print("Error: No " + str(issue['Id']) +" -> " + str(iid_issue) )
            finally: self.make_normal(user)
            
    def _closed_issues(self, issues, issue, iid_issue):
        is_closed = issues.is_closed(issue)
        if not is_closed:
            return
        if self.dry_run:
            print( 'would close the issue No ' + str(issue['Id']) +' -> ' + str(iid_issue)  )
        else:
            print("Closing Issue No " + str(issue['Id']) +" -> " + str(iid_issue) )
            if self.gl.editissue(self.project_id, iid_issue, state_event='close') is False:
                raise RuntimeError('failed to close issue %(iid)s %d' % (issue,iid_issue))

    def create_issues(self, issues):
        for issue in issues:
            print(issue['Id'])
            self._create_issue(issues, issue)
        if self.issues_csv is not None:
            with open(self.issues_csv,'w',newline='') as file:
                writer=csv.DictWriter(file,dialect='unix',
                                      extrasaction='ignore',
                                      fieldnames=['Id','__gitlab_id'])
                writer.writeheader()
                for issue in issues:
                    writer.writerow(issue)


    def make_relations(self,issues):
        for issue in issues:
            relations = issues.get_relations(issue['Id'])
            for (name, target_id) in relations:
                my_iid = issue['__gitlab_id']
                if name == 'child of' or name == 'duplicate of' or name == 'related to':
                    print(f"establishing relation between {issue['Id']} and {target_id}")
                    if target_id in issues:
                        target_iid = issues[target_id]['__gitlab_id']
                        note = f'{name} #{target_iid}'
                        gl.createissuelink(self.project_id,my_iid,
                                           target_project_id=self.project_id,
                                           target_issue_iid=target_iid)
                    else:
                        print(f"cannot link to non-existing mantis issue {target_id}")

def _argparser():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--gitlab', type=str, required=True,
                        help='gitlab base url')
    parser.add_argument('-i', '--issues', type=str, required=True,
                        help='csv file to read mantis issues from')
    parser.add_argument('-k', '--mantis-token', type=str, required=True,
                        help='mantis token')
    parser.add_argument('-m', '--mantis-base', type=str, required=True,
                        help='base Mantis URL to use for linking back to Mantis')
    parser.add_argument('-n', '--dry-run', action='store_true',
                        help='do not actually submit issues')
    parser.add_argument('-o', '--output-rel-file', type=str,
                        help='output the relation between mantis ID and gitlab IID in the given file')
    parser.add_argument('-p', '--project', type=str, required=True,
                        help='gitlab project')
    parser.add_argument('-s','--sudo',action='store_true',
                        help='use sudo requests (requires gitlab token with admin rights)')
    parser.add_argument('-t', '--token', type=str, required=True,
                        help='gitlab token')
    parser.add_argument('-u', '--mantis-user', type=str, required=True,
                        help='mantis username')
    return parser

if __name__ == '__main__':
    parser = _argparser()
    args = parser.parse_args()

    with open(args.issues, 'r') as fin:
        mantis_issues = MantisIssues(fin, args.mantis_base,args.project,
                                     args.mantis_user,args.mantis_token)

    # To have users map from Mantis to GitLab, add users mappings:
    #    mantis_issues.add_user_mapping(mantis_display_name, gitlab_username)

    # To add labels based on the category, priority, severity, project, and
    # status:
    #mantis_issues.add_category_mapping(category, ['label1', 'label2'])

    #    mantis_issues.add_project_mapping(project, ['label1', 'label2'])

    # To have status/resolution pairs cause issues to be closed, use:
    #   The 'closed' status will close regardless of the resolution:
    mantis_issues.add_closed_status(status='closed')
    mantis_issues.add_closed_status(status='resolved')
    #   The 'cantfix' resolution will close regardless of the status:
    #    mantis_issues.add_closed_status(resolution='cantfix')
    #   The 'backlog'/'expired' status/resolution will close the issue:
    #    mantis_issues.add_closed_status(status='backlog', resolution='expired')

    gl = gitlab.Gitlab(args.gitlab, args.token)
    with Mantis2Gitlab(gl, args.project, dry_run=args.dry_run,
                       sudo=args.sudo,ofile=args.output_rel_file) as m2gl:
        print("Create issues ...")
        m2gl.create_issues(mantis_issues)
        print("Add relationships between issues")
        m2gl.make_relations(mantis_issues)
